﻿using System;
using System.Diagnostics;

using Windows.Foundation.Metadata;
using Windows.UI.Xaml;

namespace Trogon.UI.Triggers {
	public class IsApiContractPresentTrigger : StateTriggerBase {
		private string _apiContractName;
		private ushort _majorVersion;
		private bool _isPresent;

		private bool? _isApiContractPresent = null;

		public string ApiContractName {
			get => _apiContractName;
			set {
				_apiContractName = value;
				UpdateState( );
			}
		}

		public int MajorVersion {
			get => _majorVersion;
			set {
				_majorVersion = Convert.ToUInt16(value);
				UpdateState( );
			}
		}

		public bool IsPresent {
			get => _isPresent;
			set {
				_isPresent = value;
				UpdateState( );
			}
		}

		private void UpdateState( ) {
			if (_apiContractName != default(string) && _majorVersion != default(ushort)) {
				if (_isApiContractPresent == null) {
					_isApiContractPresent = ApiInformation.IsApiContractPresent(_apiContractName, _majorVersion);
					Debug.WriteLine($"{_apiContractName} {_majorVersion} - {_isApiContractPresent}");
				}
			}
			SetActive(_isPresent == _isApiContractPresent);
		}
	}
}
