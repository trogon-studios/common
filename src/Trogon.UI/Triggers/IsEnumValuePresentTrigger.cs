﻿using System.Diagnostics;

using Windows.Foundation.Metadata;
using Windows.UI.Xaml;

namespace Trogon.UI.Triggers {
	public class IsEnumValuePresentTrigger : StateTriggerBase {
		private string _enumTypeName;
		private string _enumValueName;
		private bool _isPresent;

		private bool? _isEnumValuePresent = null;

		public string EnumTypeName {
			get => _enumTypeName;
			set {
				_enumTypeName = value;
				UpdateState( );
			}
		}

		public string EnumValueName {
			get => _enumValueName;
			set {
				_enumValueName = value;
				UpdateState( );
			}
		}

		public bool IsPresent {
			get => _isPresent;
			set {
				_isPresent = value;
				UpdateState( );
			}
		}

		private void UpdateState( ) {
			if (_enumTypeName != default(string) && _enumValueName != default(string)) {
				if (_isEnumValuePresent == null) {
					_isEnumValuePresent = ApiInformation.IsEnumNamedValuePresent(_enumTypeName, _enumValueName);
					Debug.WriteLine($"{_enumTypeName} {_enumValueName} - {_isEnumValuePresent}");
				}
				SetActive(_isPresent == _isEnumValuePresent);
			}
		}
	}
}
