﻿using System.Diagnostics;

using Windows.Foundation.Metadata;
using Windows.UI.Xaml;

namespace Trogon.UI.Triggers {
	public class IsPropertyPresentTrigger : StateTriggerBase {
		private string _typeName;
		private string _propertyName;
		private bool _isPresent;

		private bool? _isPropertyPresent = null;

		public string TypeName {
			get => _typeName;
			set {
				_typeName = value;
				UpdateState( );
			}
		}

		public string PropertyName {
			get => _propertyName;
			set {
				_propertyName = value;
				UpdateState( );
			}
		}

		public bool IsPresent {
			get => _isPresent;
			set {
				_isPresent = value;
				UpdateState( );
			}
		}

		private void UpdateState( ) {
			if (_typeName != default(string) && _propertyName != default(string)) {
				if (_isPropertyPresent == null) {
					_isPropertyPresent = ApiInformation.IsPropertyPresent(_typeName, _propertyName);
					Debug.WriteLine($"{_typeName} {_propertyName} - {_isPropertyPresent}");
				}
				SetActive(_isPresent == _isPropertyPresent);
			}
		}
	}
}
